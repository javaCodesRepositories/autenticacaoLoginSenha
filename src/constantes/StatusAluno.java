package constantes;

/* CLASSE E CONSTANTE ESTATICAS */
public class StatusAluno {

	public static String APROVADO = "Aluno Aprovado!";
	public static String RECUPERACAO = "Aluno em recuperacao!";
	public static String REPROVADO = "Aluno Reprovado!";
}
